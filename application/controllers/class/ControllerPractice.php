<?
class ControllerPractice extends CI_Controller{

    // 동적폼전송 - 실습3
    public function pocketmon(){
        $this->load->view("class/viewPractice");
    }

    public function capture_mon(){
        $this->load->model("model_tools");

        if( $_POST["pocket_mon"]!=NULL ){
            $date = date("Y-m-d H:i:s");

            // 배열변수 
            // $_POST로 전달받은 ["pocket_mon"]을 1번째, $date를 2번째 인수로 전달
            $this->model_tools->capture($_POST["pocket_mon"], $date);

            echo $date."에 ".$_POST["pocket_mon"]." 을(를) 획득하였다!";
        }
        else{
            echo "포획한 포켓몬 이름을 입력하셔야 해요!";
            return false;
        }
    }

    public function call_mon(){
        $this->load->model("model_tools");

        $my_mon = $_POST["my_mon"];
        $result = $this->model_tools->call($my_mon);

        if($result-> num_rows()==0) echo $_POST["my_mon"]."은(는) 포획한 적이 없다;";
        else echo $_POST["my_mon"]." 소환 성공";

    }


    // 동적폼전송 - 실습2
    public function input_msg()
    {
        echo $_POST["msg"];
    }

    public function view_msg(){
        $this->load->view("class/viewPractice");
    }



    public function view()
    {
        // 배열변수 $data 정의
        // $data의 인덱스로 a, b 사용 
        $data = array(
            "a" => "hi",    // viewPractice.php 파일에서의 $a
            "b" => "hello"  // viewPractice.php 파일에서의 $b
        );

        // viewPractice를 출력하면서 2번째 인수로 $data라는 배열변수를 전달
        // (viewPractice.php를 호출할 때, $data를 함께 넘겨줌)
        $this->load->view("class/viewPractice",$data);
        
        // Controller 에서 view를 호출할 때 함께 전달한 배열변수의 index를 
        // view에서 변수로 사용할 수 있다
    }

    public function arg_test($a) // IP주소/controllerPractice/arg_test/hello
                                // 컨트롤러의 arg_test라는 메소드를 호출하며, 
                                // 그 첫번째 인자 $a에 본인이 치는 주소값(hello)을 할당하라는 의미
    {
        echo $a;    //화면에 hello 출력
    }

    public function arg_test2( $a, $b, $c ) // 101.101.219.198//controllerPractice/arg_test/hello/bye/no
    {
        echo $a." ".$b." ".$c;    //화면에 hello bye no 출력
    }

}
?>
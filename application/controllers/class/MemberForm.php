<?
class MemberForm extends CI_Controller{

    public function delete_member()
    {
        $this->load->model("memberModel");
        if($this->memberModel->delete_member()) echo "회원 탈퇴 성공";
        else echo "회원 탈퇴 실패. 오류 발생!";
    }

    public function update_pw()
    {
        print_r($_POST);
        $this->load->model("memberModel");
        if($this->memberModel->update()) echo "비밀번호 변경 성공";
        else echo "비번 변경 실패. 오류 발생!";
    }

    public function select_member()
    {
        echo "<form action = 'http://101.101.219.198/class/memberForm/update_pw' method='post'>";

        $this->load->model("memberModel");
        $result=$this->memberModel->get_memberList_all();

        echo "<table border-collapse='collapse'>"; 
        echo "<tr>";
        echo "<td>"." "."</td>";
        echo "<td>"."회원ID"."</td>";
        echo "</tr>";

        foreach($result->result_array() as $row)
        {
            echo "<tr>";
            echo "<td>"."<input type='radio' name='id' value='".$row["id"]."'>"."</td>";
            echo "<td>".$row["id"]."</td>";
            echo "</tr>";    
        }
        echo "</table>";
        echo "<button type='submit'>변경하기</button>";
        echo "</form>";
    }

    public function read_member_3()
    {
        $this->load->model("memberModel");
        $result = $this->memberModel->get_memberList_3();

        echo "<table border-collapse='collapse'>"; 
        echo "<tr>";
        echo "<td>"."No."."</td>".
            "<td>"."회원ID"."</td>".
            "<td>"."비밀번호"."</td>".
            "<td>"."이름"."</td>".
            "<td>"."성별"."</td>".
            "<td>"."생년월일"."</td>";
        echo "</tr>";   

        foreach($result->result_array() as $row)
        {
            echo "<tr>";
            echo "<td>".$row["no"]."</td>".
                "<td>".$row["id"]."</td>".
                "<td>".$row["pw"]."</td>".
                "<td>".$row["name"]."</td>".
                "<td>".$row["gender"]."</td>".
                "<td>".$row["birthday"]."</td>";
            echo "</tr>";    
        }

        echo "</table>";
    }

    public function read_90s_female()
    {
        $this->load->model("memberModel");
        $result=$this->memberModel->get_memberList_90s_female();
        echo "<table border-collapse='collapse'>"; 
        echo "<tr>";
        echo "<td>"."No."."</td>".
            "<td>"."회원ID"."</td>".
            "<td>"."비밀번호"."</td>".
            "<td>"."이름"."</td>".
            "<td>"."성별"."</td>".
            "<td>"."생년월일"."</td>";
        echo "</tr>";   

        foreach($result->result_array() as $row)
        {
            echo "<tr>";
            echo "<td>".$row["no"]."</td>".
                "<td>".$row["id"]."</td>".
                "<td>".$row["pw"]."</td>".
                "<td>".$row["name"]."</td>".
                "<td>".$row["gender"]."</td>".
                "<td>".$row["birthday"]."</td>";
            echo "</tr>";    
        }

        echo "</table>";
    }


    public function read_90s_male_data()
    {
        $this->load->model("memberModel");
        $result=$this->memberModel->get_memberList_90s_male();
        echo "<table border-collapse='collapse'>"; 
        echo "<tr>";
        echo "<td>"."No."."</td>".
            "<td>"."회원ID"."</td>".
            "<td>"."비밀번호"."</td>".
            "<td>"."이름"."</td>".
            "<td>"."성별"."</td>".
            "<td>"."생년월일"."</td>";
        echo "</tr>";   

        foreach($result->result_array() as $row)
        {
            echo "<tr>";
            echo "<td>".$row["no"]."</td>".
                "<td>".$row["id"]."</td>".
                "<td>".$row["pw"]."</td>".
                "<td>".$row["name"]."</td>".
                "<td>".$row["gender"]."</td>".
                "<td>".$row["birthday"]."</td>";
            echo "</tr>";    
        }

        echo "</table>";
    }


    public function read_90s_data()
    {
        $this->load->model("memberModel");
        $result=$this->memberModel->get_memberList_90s();

        echo "<table border-collapse='collapse'>"; 
        echo "<tr>";
        echo "<td>"."No."."</td>".
            "<td>"."회원ID"."</td>".
            "<td>"."비밀번호"."</td>".
            "<td>"."이름"."</td>".
            "<td>"."성별"."</td>".
            "<td>"."생년월일"."</td>";
        echo "</tr>";   

        foreach($result->result_array() as $row)
        {
            echo "<tr>";
            echo "<td>".$row["no"]."</td>".
                "<td>".$row["id"]."</td>".
                "<td>".$row["pw"]."</td>".
                "<td>".$row["name"]."</td>".
                "<td>".$row["gender"]."</td>".
                "<td>".$row["birthday"]."</td>";
            echo "</tr>";    
        }

        echo "</table>";
    }


    public function read_male_data()
    {
        $this->load->model("memberModel");
        $result=$this->memberModel->get_memberList_male();

        echo "<table border-collapse='collapse'>"; 
        echo "<tr>";
        echo "<td>"."No."."</td>".
            "<td>"."회원ID"."</td>".
            "<td>"."비밀번호"."</td>".
            "<td>"."이름"."</td>".
            "<td>"."성별"."</td>".
            "<td>"."생년월일"."</td>";
        echo "</tr>";   

        foreach($result->result_array() as $row)
        {
            echo "<tr>";
            echo "<td>".$row["no"]."</td>".
                "<td>".$row["id"]."</td>".
                "<td>".$row["pw"]."</td>".
                "<td>".$row["name"]."</td>".
                "<td>".$row["gender"]."</td>".
                "<td>".$row["birthday"]."</td>";
            echo "</tr>";    
        }

        echo "</table>";
    }


    public function read_member_data()
    {
        $this->load->model("memberModel");
        $result=$this->memberModel->get_memberList_all();

        //cellspacing: 테이블과 칸(td)사이의 여백 지정.
        //cellpadding: 내용(글자)와 보더 사이의 공간. th 또는 td셀의 여백 지정.
        echo "<table border=1 cellpadding=0 cellspacing=0>"; 
        echo "<tr>";
        echo "<td>"."No."."</td>".
            "<td>"."회원ID"."</td>".
            "<td>"."비밀번호"."</td>".
            "<td>"."이름"."</td>".
            "<td>"."성별"."</td>".
            "<td>"."생년월일"."</td>";
        echo "</tr>";    

        foreach($result->result_array() as $row)
        {
            echo "<tr>";
            echo "<td>".$row["no"]."</td>".
                "<td>".$row["id"]."</td>".
                "<td>".$row["pw"]."</td>".
                "<td>".$row["name"]."</td>".
                "<td>".$row["gender"]."</td>".
                "<td>".$row["birthday"]."</td>";
            echo "</tr>";    
        }

        echo "</table>";
    }


    // 회원전용 main 페이지
    public function member_main()
    {
        $this->load->view("class/member_main"); 
    }
  
    public function member_signup()
    {
        // member_signup.php를 출력하기 위한 메소드
        $this->load->view("class/member_signup"); // 사용하기 위해 view를 불러옴
    }

    // 폼으로부터 전송된 $_POST["id" "pw" "name" "gender" "birthday_year" "birthday_month" "birthday_day"]을 
    // MemberModel라는 모델의 data_insert 메소드에 전달.
    public function insert_member_data()
    {
        $this->load->model("memberModel"); //Model을 사용하기 위해 Model을 불러옴

        //$result=$this->memberModel->
        //data_insert($_POST를 호출할 때 인자로 $_POST["id" "pw" "name"..]을 넘겨줌. <- DB로 이 값을 입력할 것.
        //$result=
        // $this->memberModel->data_insert(
        //     $_POST["id"],
        //     $_POST["pw"],
        //     $_POST["name"],
        //     $_POST["gender"],
        //     $_POST["birthday_year"],
        //     $_POST["birthday_mon"],
        //     $_POST["birthday_day"]
        // );

        if($this->memberModel->data_insert( 
            $_POST["id"],
            $_POST["pw"],
            $_POST["name"],
            $_POST["gender"],
            $_POST["birthday_year"],
            $_POST["birthday_mon"],
            $_POST["birthday_day"])) echo "입력이 완료되었습니다.";
        else echo "알 수 없는 오류가 발생했습니다.";


        // foreach($result->result_array() as $row)
        // {
        //     echo $row["id"]." ".$row["pw"]." ".$row["name"]." ".$row["gender"]." "
        //     .$row["birthday_year"]."-".$row["birthday_mon"]."-".$row["birthday_day"]."<br>";
        // }


    }

}
?>
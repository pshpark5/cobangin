<?
defined('BASEPATH') or exit('No direct script access allowed');

class TestController extends CI_Controller{

    // 2개의 메소드를 정의
    public function a()
    {
        echo "a메소드에 입력한 값 출력 확인";
    }

    public function b()
    {
        echo "b메소드에 입력한 값 출력 확인";
    }

    public function view(){
        $this->load->view( "class/test_view" );
    }
}
?>
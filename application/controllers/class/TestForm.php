<?
class TestForm extends CI_Controller{

    public function validation()
    {
        $this->load->view("class/form_validation");
    }

    public function form_validation()
    {
        $data= array(
            "id" => $_POST["id"],
            "name" => $_POST["name"],
            "pw" => $_POST["pw"]
        );
        $this->load->view("class/form_received",$data);
    }


    public function dynamic()
    {
        $this->load->view("class/form_dynamic");
    }

    public function form_dynamic()
    {
        if($_POST["msg"]!=null) echo "성공했당";
        else echo "실패. 못했다";
    }

    public function form()
    {
        //form을 출력하기 위한 메소드
        $this->load->view("class/form");
    }

    public function process_post_data()
    {
    // application/views/form.php로부터 전송된 데이터를 처리하기 위한 메소드
    $this->load->model("model_tools");

    $result=$this->model_tools->get_by_test($_POST["name"]);

    foreach($result->result_array() as $row)
    {
        echo $row["no"]." ".$row["id"]." ".$row["name"]."<br>";
    }

    }
}
?>
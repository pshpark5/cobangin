<?
 
 class TestModel extends CI_Controller {

  public function index()
  {
   $this->load->model("model_tools"); // Model을 사용하기 위해 Model을 불러옴
    // Model_tools.php의 파일명이 대문자로 시작해도 소문자로 입력해서 불러와도 됨.
  
    // 위에서 Model을 호출해왔으므로
    // 여기부터는 Model이 지니고 있는 메소드를 사용할 수 있음
    // get_test()메소드 호출: select문의 결과 반환
   $result = $this->model_tools->get_test();

   //$result_all = $this->model_tools->get_all_test();
   
   //foreach : 특정 조건이 달성될때까지 반복 
   //           php내장함수 중 하나로, 특정 배열변수에 저장된 값을 모두 불러올 때까지 반복문을 실행
   // $result->result_array() as $row : foreach반복문을 돌며
   //                                   $result 변수에 저장된 값들을 하나씩 $row 변수에 할당
   // result 변수에는 select문을 이용해 DB에서 검색된 레코드들이 저장되어 있고
   // 이 레코드들을 하나씩 번갈아 row변수에 할당
   // 모든 레코드들이 할당되면 foreach문/함수가 종료
   foreach ( $result->result_array() as $row ){ 
    echo $row["no"]." ".$row["id"]." ".$row["name"]."<br>";
   }

//    foreach ( $result_all->result_array() as $row ){ 
//     echo $row["id"]." ".$row["name"]."<br>";
//    }
   
  }
  
 }
  
 ?>
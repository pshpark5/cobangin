<?
 // [실습]
 // 해킹과보안: CodeIgniter Controller, View: 34~35쪽
 // Controller(TestModel2.php)도 출력부분만 따로 빼내어 view파일 나눔
 // Controller에서 변수 $result를 view에게 전달해 출력을 위임
 class TestModel2 extends CI_Controller {

  public function index()
  {
   $this->load->model("model_tools"); // Model을 사용하기 위해 Model을 불러옴
   //-------------
   //방법1
   $query = $this->model_tools->get_test(); // sql_practice 테이블의 select문 조회 레코드 반환
   $result['list'] = $query->result_array(); // 조회한 결과 데이터(=레코드)들을 array/배열 형태로 재정렬 
   
   //방법2
   //$result['list'] = $this->model_tools->get_test(); // sql_practice 테이블의 select문 조회 결과 반환 
   //-------------

   $this->load->view("class/test_view", $result);
 
  }
  
 }
  
 ?>
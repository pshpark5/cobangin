<?
class MemberModel extends CI_Model 
{
    //delete: id "jungkrat"인 정크랫 회원 탈퇴 처리
    public function delete_member()
    {
        $this->db->where("id","jungkrat");
        //$this->db->limit(1);
        if($this->db->delete("sql_practice")) return true;
        else return false;
    }

    // update: 전달받은 id 값에 해당되는 pw 12345678로 변경
    public function update()
    {
        $this->db->where("id", $_POST["id"]);
        //$this->db->limit(1);
        $set=array(
            "pw"=>"12345678"
        );
        
        if($this->db->update("sql_practice", $set)) return true;
        else return false;
    }

    //update: id 컬럼의 값이 hong1234인 레코드의 pw컬럼의 값을 12345678로 변경
    // public function update()
    // {
    //     $this->db->where("id","hong1234");
    //     //$this->db->limit(1);
    //     $set=array(
    //         "pw"=>"12345678"
    //     );
        
    //     if($this->db->update("sql_practice", $set)) return true;
    //     else return false;
    // }

    // 첫 3개의 레코드만 출력 : limit 함수 사용
    public function get_memberList_3()
    {
        $this->db->select("no, id, pw, name, gender, birthday"); // 전체 칼럼 선택
        $this->db->order_by("birthday","asc"); // 생년월일 기준으로 정렬
        $this->db->limit(3); // 출력할 레코드 길이 지정 (Produces: LIMIT 3)
        return $this->db->get("sql_practice");
    }


    //여성이거나 90년대생인 (여성 OR 90년대) 회원 데이터
    public function get_memberList_90s_female()
    {
        $this->db->select("no, id, pw, name, gender, birthday"); // 전체 칼럼 선택
        $this->db->where("gender","여");
        //or_ 로 다중조건 지정. or_where  or_like 등
        $this->db->or_like("birthday","199","after"); 
        $this->db->order_by("birthday","ASC");
        return $this->db->get("sql_practice");
    }

    //90년대생 남성 회원 데이터
    public function get_memberList_90s_male()
    {
        $this->db->select("no, id, pw, name, gender, birthday"); // 전체 칼럼 선택
        $this->db->where("gender","남");
        $this->db->like("birthday","199","after");
        $this->db->order_by("birthday","ASC");
        return $this->db->get("sql_practice");
    }

    // sql_practice 테이블에 있는 90년대생 데이터 불러오기
    //                           6월생
    public function get_memberList_90s()
    {
        $this->db->select("no, id, pw, name, gender, birthday"); // 전체 칼럼 선택
        $this->db->like("birthday","-06-");
        //$this->db->like("birthday","199"); //birthday 속성값이 199x-xx-xx인 회원목록
        // Codeigniter에서는 LIKE문을 사용할 때 "%" 없이 "199" 라고 해야 합니다.
        // "199%-%%-%%" 라고 입력할 시 "%"도 문자열로 인식해버리기 때문입니다.
        return $this->db->get("sql_practice");
    }

    // sql_practice 테이블에 있는 남성 데이터 불러오기
    public function get_memberList_male()
    {
        $this->db->select("no, id, pw, name, gender, birthday"); // 전체 칼럼 선택
        $this->db->where("gender","남"); //gender속성값이 "남"인 회원목록만 가져옴
        $this->db->order_by("birthday","asc"); // 생년월일 기준으로 정렬
        return $this->db->get("sql_practice");
    }
    
    // sql_practice 테이블에 있는 모든 데이터 불러오기
    public function get_memberList_all()
    {
        $this->db->select("no, id, pw, name, gender, birthday"); // 전체 칼럼 선택
        $this->db->order_by("birthday","asc"); // 생년월일 기준으로 정렬
        return $this->db->get("sql_practice");
    }

    // data_insert 메소드로 처리된 (삽입된)
    // DB값 조회, 출력하기
    //public function data_insert($a, $b){
    public function data_insert($id,$pw, $name,$gender,$birthday_year,$birthday_mon,$birthday_day){    
        $data = array(
            array(
                "id" => $id,
                "pw" => $pw,
                "name" => $name,
                "gender" => $gender,
                "birthday" => $birthday_year."-".$birthday_mon."-".$birthday_day
            )
        );

        return $this->db->insert_batch("sql_practice",$data);
    }
}
?>
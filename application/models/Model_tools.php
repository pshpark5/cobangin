<? if ( ! defined('BASEPATH') ) exit('No direct script access allowed');
 
class Model_tools extends CI_Model {

    // 동적폼전송_실습3(폭행몬)
    public function capture($pocket_mon, $date){
        $data = array(
            'name'=>$pocket_mon,
            'date'=>$date
        );
        
        // 'pocketmon'이라는 테이블에 
        // $data 배열(새로운 데이터 row 삽입)
        $this->db->insert( 'pocketmon', $data );
    } 
    
    public function call($my_mon){
        
        //where문을 통해 이름이 인자로 넘어온 
        $this->db->where("name",$my_mon);
        
        //$my_mon인 것을 찾아 그 결과를 return
        $result=$this->db->get("pocketmon"); // 'pocketmon'테이블에서 get함
        return $result;
    }
    // /END/ 동적폼전송_실습3(폭행몬)

 

    
 public function get_test() // select문의 결과 반환
 {
  $this->db->select("no, id, name"); // 생략 가능
  
  $this->db->where("no>=", 3); // 생략 가능
  
  return $this->db->get("sql_practice"); // 위의 조건 생략 시, 테이블의 모든 레코드 출력
                                            // ip주소/test/index 접속 시 확인 가능
 
  //위의 코드는 아래의 sql문을 실행한 것과 같습니다.
  //SELECT 칼럼1, 칼럼2 FROM 테이블명 WHERE 칼럼1 = 1
 }

//  // 모든 레코드 출력 
//  public function get_all_test()
//  {
//   return $this->db->get("sql_practice");
//  }


// TestForm.php 컨트롤러에서 전달된 $_POST["name"]값을
// get_by_test 메소드로 처리
// $_POST["name"]값으로 조회된 DB값 출력하기 
public function get_by_test($post_value) //$post_value 라는 인자를 입력받음.  
{
    $this->db->select("no, id, name"); // [형식] "칼럼1, 칼럼2, 칼럼3"   (<- 오류발생: "칼럼1", "칼럼2")
    $this->db->where("name", $post_value); // 칼럼을 검색하기 위한 조건으로 $post_value를 사용.

    return $this->db->get("sql_practice"); //테이블명
}

}

?>
<!-- 동적 폼 전송 공부 -->
<html>
 <head>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
 
  <script>
   // 폼태그 전송
   // <form action="http://내홈페이지주소/test/dynamic_form" method="post">
   // <input type="text" name="msg" value="야호">
   // </form>

   // 동적 폼전송 (위의 코드와 동일한 작동,,)  
   function go(){
    // 동적 폼 전송 시, $.post나 $.get을 사용   
    $.post(
        "http://101.101.219.198/class/testForm/form_dynamic", // 데이터를 전송할 곳. 즉, action을 기록하는 곳.
        { msg : "야호", msg2: "야호2" }, // 보낼 데이터들을 정의하는 부분. 
        // name=msg, value="야호"라는 데이터를 서버로 전송..
        // 서버에서는 $_POST["msg1"], $_POST["msg2"]로 수신하면 됨.
        function( data ){ // 서버로부터 전송된 데이터. 
                        // msg에 "야호"라는 값이 제대로 전달되면 
                        // "성공했당"이 data변수에 저장됨.
        alert( data ); // "성공했당" 문구 확인 경고창
        }
    );
   }


   // span태그에 적용
    function change(){
        alert( document.getElementById( "my_span" ).innerHTML );
        
        // document(문서)에서 my_span이라는 ID에 의해
        // Element(요소)를 get(가져올 것)하라는 뜻
        document.getElementById( "my_span" ).style.backgroundColor = "red";
        document.getElementById( "my_span" ).innerHTML = "반갑습니다."; 
        // innerHTML: id(my_span)에 해당되는 텍스트를 .innerHTML ="새텍스트"의 새텍스트로 변경 
        //  ㄴ  해당 태그 안에 코드를 삽입하는 명령어
        // input태그 등의 폼 요소에도 innerHTML을 사용할 수 있나? 
        // No. 폼 요소에는 value명령어를 사용
    }
    
    // input태그에 적용
    function go2()
    {
        $.post(
            "http://101.101.219.198/class/testForm/form_dynamic",
            { msg : document.getElementById( "my_input" ).value },
               function( data ){
                alert( data );
               }
        );
    }

  </script>
  
 </head>
 
 <body>
 <input type="text" id="my_input">	

 <span id="my_span" onclick="change();">안녕하세요.</span>

 <!--버튼의 onclick속성에 함수를 지정해 실행시키기 위해서는
 반드시 type을 버튼으로 설정해줘야 함.
 만약 submit으로 지정하거나 생략하면 
 이 버튼이 속한 form이 자동으로 전송되기 때문에
 onclick에 지정된 함수가 실행되지 않음. -->   
 <button type="button" onclick="go2();">전송</button>
 
 <!--go 함수를 호출하기 위한 버튼-->
  <!-- <button type="button" onclick="go();">전송</button> -->
 </body>
</html>
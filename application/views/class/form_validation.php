<html>
<head>
<title>회원가입 유효성검사</title>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>

    // 실습1 방식 : 일반 폼으로 구현
    // function check(){
    //     var id = document.getElementById("id").value;
    //     var name = document.getElementById("name").value;
    //     var pw = document.getElementById("pw").value;

    //     if(id =="" || name =="" || pw =="" ){
    //         alert("id, 이름, 비번을 빠짐없이 입력해주세요");
    //         return false;
    //     }
        
    //     document.getElementById("my_form").submit();
    //     alert("회원가입이 완료되었습니다!");
    // }
    
    
    // 실습3 방식: 동적 폼으로 구현
    function check_3rd()
    {
        var formData = document.getElementById("my_form_3rd");
        if(!formData.checkValidity())
        {
            formData.reportValidity();
            return false;
        }        

        $.post("http://101.101.219.198/class/testForm/form_validation",
            {id: document.getElementById("id").value,
            name: document.getElementById("name").value,
            pw: document.getElementById("pw").value},
 
            function(data){
                alert(data);
            }
        );
    }

</script>
</head>
<body>

<!-- 실습 3: 동적 폼 전송방식 + html5 validation 내장함수(required) 사용 -->
<form id="my_form_3rd">
    <div>
        아이디 
        <input type="text" name="id" id="id" required>
    </div>
    <div>
        이름 
        <input type="text" name="name" id="name" required>
    </div>
    <div>
        비밀번호 
        <input type="text" name="pw" id="pw" required>
    </div>

    <button type="button" onclick="check_3rd();">
        회원가입!
    </button>
</form>

<!-- 
    <form action="http://101.101.219.198/testForm/form_validation" method="post" id="my_form" >
        <table>
            <tr>
                <td>아이디</td>

                실습1 방식
                <td><input type="text" id="id" name="id"></td>

                <실습2 : html5 validation 내장함수(required) 추가>
                <td><input type="text" id="id" name="id" required></td>
            </tr>
            <tr>
                <td>이름</td>

                실습1 방식
                <td><input type="text" id="name" name="name"></td>

                <실습2 : html5 validation 내장함수(required) 추가>
                <td><input type="text" id="name" name="name" required></td>
            </tr>
            <tr>
                <td>비밀번호</td>

                실습1 방식
                <td><input type="password" id="pw" name="pw"></td>
                
                <실습2 : html5 validation 내장함수(required) 추가>
                <td><input type="password" id="pw" name="pw" required></td>
            </tr>
        </table>
    
    실습1 방식
    <button type="button" onclick="check();">회원가입!</button>

    <실습2: html5 validation 내장함수(required)는, submit 클릭시에만 실행됨>   
    <button type="submit">회원가입!</button>
    </form> 
-->

</body>
</html>
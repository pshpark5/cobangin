<html>
<head>
<meta charset="utf-8">
    <title>CodeIgniter 모델 실습 1__회원명부 입력</title>
</head>

<body>
<!--action이 가리키는 url로 post 방식으로 정보 전송-->
<form action="http://101.101.219.198/class/memberForm/insert_member_data" method="post">
    <div class="form-row align-items-center mb-2">
        <label class="col-auto">ID</label> 
        <input type="text" name="id" placeholder="4자이상 입력" size="10">
    </div>
    <div class="form-row align-items-center mb-2">
        <label class="col-auto">비번</label> 
        <input type="password" name="pw" placeholder="숫자+영어" size="10">
    </div>
    <div class="form-row align-items-center mb-2">
        <label class="col-auto">이름</label> 
        <input type="text" name="name" placeholder="본명" size="10">
    </div>
    <div class="form-row align-items-center mb-2">
        <label class="col-auto">성별</label> 
            <input type="radio" name="gender" value="남" size="10" checked>남
            <input type="radio" name="gender" value="여" size="10">여
    </div>
    <div class="form-row align-items-center mb-2">
        <label class="col-auto">생년월일</label> 
            <select name="birthday_year">
            <?
                for( $year = 1970; $year < 2020; $year++ ){
                echo "<option value='".$year."'>".$year."</option>";
                }
            ?>
            </select>년
            <select name="birthday_mon">
            <?
                for( $month = 1; $month < 13; $month++ ){
                echo "<option value='".$month."'>".$month."</option>";
                }
            ?>
            </select>월
            <select name="birthday_day">
            <?
                for( $day = 1; $day < 32; $day++ ){
                echo "<option value='".$day."'>".$day."</option>";
                }
            ?>
            </select>일
    </div>

    <div class="form-row align-items-center">
        <button type="submit">회원등록하기</button>
    </div>
</form>
</body>
</html>
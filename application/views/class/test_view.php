<!-- test_view.php -->
 
<!DOCTYPE html>
 
<html>
 <head>
  <title>테스트</title>
 </head>
 
 <body>
  오호라?
  <br>
  
  <?
  //foreach : 특정 조건이 달성될때까지 반복 
   //           php내장함수 중 하나로, 특정 배열변수에 저장된 값을 모두 불러올 때까지 반복문을 실행
   // $result->result_array() as $row : foreach반복문을 돌며
   //                                   $result 변수에 저장된 값들을 하나씩 $row 변수에 할당
   // result 변수에는 select문을 이용해 DB에서 검색된 레코드들이 저장되어 있고
   // 이 레코드들을 하나씩 번갈아 row변수에 할당
   // 모든 레코드들이 할당되면 foreach문/함수가 종료
   
   //TestModel2.php출력
   //----------------------------------------------
   // [참고] http://elenore.woobi.co.kr/day5/ex04
   // 방법1
   foreach ( $list as $row ){  
    echo $row["no"]." ".$row["id"]." ".$row["name"]."<br>";
   }   
   // 방법2
   //foreach ( $list->result_array() as $row ){  
   // echo $row["no"]." ".$row["id"]." ".$row["name"]."<br>";
   //}   

   ?>
 </body>
</html>

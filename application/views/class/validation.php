<html>
<head>
<title>회원가입 유효성검사</title>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>
    function check(){
        var id = document.getElementById("id").value;
        var name = document.getElementById("name").value;
        var pw = document.getElementById("pw").value;

        if(id =="" || name =="" || pw =="" ){
            alert("id, 이름, 비번을 빠짐없이 입력해주세요");
            return false;
        }
        
        document.getElementById("my_form").submit();
        alert("회원가입이 완료되었습니다!");
    }
</script>
</head>
<body>
    <form action="http://101.101.219.198/testForm/form_validation" method="post" id="my_form" >
        <table>
            <tr>
                <td>아이디</td>
                <td><input type="text" id="id" name="id"></td>
            </tr>
            <tr>
                <td>이름</td>
                <td><input type="text" id="name" name="name"></td>
            </tr>
            <tr>
                <td>비밀번호</td>
                <td><input type="password" id="pw" name="pw"></td>
            </tr>
        </table>
    <!-- 실습1 해답 -->
    <button type="button" onclick="check();">회원가입!</button>
    </form>
</body>
</html>
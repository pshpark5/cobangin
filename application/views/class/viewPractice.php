<!-- CodeIgniter View -->	
	
<html>
  <head>
    <title>View Test</title>
  </head>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>

        // 동적폼전송 - 실습1
        function copy_to_right()
        {
            document.getElementById("input_right").value=document.getElementById("input_left").value;
        }

        function copy_to_left()
        {
            document.getElementById("input_left").value=document.getElementById("input_right").value;
        }

        // ############################
        // 동적폼전송 - 실습2
        // ############################
        function red(){
            document.getElementById("palette").style.backgroundColor="red";
        }
        function blue(){
            document.getElementById("palette").style.backgroundColor="blue";
        }
        function yellow(){
            document.getElementById("palette").style.backgroundColor="yellow";
        }

        // 동적 폼전송을 이용해 
        // controller의 change 메소드에 갔다가
        // 그대로 글자를 echo로 출력해
        // view에서 id="palette"인 곳에 글자를 넣어줬음
        function input_text(){
            $.post(
                "http://101.101.219.198/class/controllerPractice/input_msg",
                {"msg" : document.getElementById("my_msg").value },
                function(server_printed_data){
                    document.getElementById("palette").innerHTML = server_printed_data;
                }
            );
        }


        // ############################
        // 동적폼전송 - 실습3
        // ############################
        function capture(){
            $.post(
                "http://101.101.219.198/class/controllerPractice/capture_mon",
                {"pocket_mon" : document.getElementById("pocket_mon").value},
                function(data){
                    alert(data);
                }
            );
        }

        function call(){
            $.post(
                "http://101.101.219.198/class/controllerPractice/call_mon",
                {"my_mon": document.getElementById("my_mon").value},
                function(data){
                    alert(data);
                }
            );
        }

    </script>
  <body>
    <!-- 실습1 -->
    <div>
        <input type="text" name="input_left" id="input_left">
        <button type="button" onclick="copy_to_right();">오른쪽으로 복제</button>
        <input type="text" name="input_right" id="input_right">
        <button type="button" onclick="copy_to_left();">왼쪽으로 복제</button>
    </div>
    <br>
    <!-- 실습2 -->
    <div>
        <button type="button" style="background-color: red; width:30px; height:30px; border:0;" onclick="red();"></button>
        <button type="button" style="background-color: blue; width:30px; height:30px; border:0;" onclick="blue();"></button>
        <button type="button" style="background-color: yellow; width:30px; height:30px; border:0;" onclick="yellow();"></button>
        <br>
        <input type="text" id="my_msg">
        <button type="button" onclick="input_text();">입력</button>
    </div>
    <br>
    <div id="palette" style="width: 30%; height:30%; text-align:center;">
    </div>

    <!-- 실습3 -->
    <div>
        폭행몬 포획!
        <br>
        <input type="text" id="pocket_mon">
        <button type="button" onclick="capture();">포획</button>
        <br>
        포켓몬 소환!
        <br>
        <input type="text" id="my_mon">
        <button type="button" onclick="call();">소환</button>
    </div>

    <?
    // $a, $b에 전달된 값이 hi, hello로 출력됨.
    //    echo $a;   
    //    echo "<br>";
    //    echo $b;
    ?>
  </body>
</html>
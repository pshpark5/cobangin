// 자바스크립트(.js)란?
// http://codingnuri.com/javascript-tutorial/what-is-javascript.html

// 각 step 클래스(말풍선)에
// data-index=0,1,2,3,...
// data-xxxxx 을 활용해 
// 그림과 텍스트(말풍선)에 일치하는 인덱스를 달아주기 위함
// (인덱스마다 복붙 대신 반복적인 기능에 대해 자바스크립트로 루프돌며 자동으로 인덱스 붙이도록 처리) \

// 즉시실행함수/익명함수
// https://osxtip.tistory.com/635
(() => {
   // 화살표 함수를 만들고 괄호로 감싼 다음에
   // 괄호 연산자로 실행하는 함수임.
   // 사용 이유 : 함수 안에서 선언되는 변수는 지역변수가 되기 때문에
   //         외부에서 접근 x.
   //          전역변수화 되는 것을 방지.
   //         전역변수는 누구나 접근 가능해서 보안상 위험+충돌 가능성도 有
   //         자바스크립트는 주로 클라이언트/브라우저 단에서 실행되므로 
   //         전역변수 사용 시, 보안상 취약 (전역변수 사용 지양할 것.)
   
   // const 선언은 블록 범위의 상수를 선언. 상수의 값은 재할당할 수 없으며 다시 선언할 수도 없다.
   // https://developer.mozilla.org/ko/docs/Web/JavaScript/Reference/Statements/const
   // https://curryyou.tistory.com/192 (var, let, const)
   const storyElems = document.querySelectorAll('.story');
   const imgElems = document.querySelectorAll('.each-img');
   //let currentImgElems; //현재 활성화된(visible 클래스가 붙은) .each-img를 지정하고 있는 변수
   
   // [문제점] for문으로 돌리면 첫번째 item 도 invisible 상태로 시작.
   // [해결] 따라서,
   // currentImgElems에 imgElems의 첫번째 item/img을 담아두고
   // .visible 클래스 붙이는 classList.add('visible')을 활성화하는 방식으로 접근
   let currentImgElems = imgElems[0];
   let ioIndex;
	
	// for문이 아이템/요소 전부 검사하니까 비효율
	// 지금은 검사할 아이템이 적지만 스크롤이 끝없이 내려가는 페이지 생각하면,, 
	// 매번 모든 아이템을 판별한다고 생각해보길,,
	// 따라서 화면에 보이는 Element+ 요소의 위, 아래 요소까지 총 3개만 검사할 것.
	// IntersectionObserver를 사용하면 현재 몇번째 index를 가진 아이템/요소가 보이는 지 확인 가능.

	// IntersectionObserver를 사용하여 화면에 노출된 아이템만 판별하기.
	// ***** IntersectionObserver란? ***** 
	// Target Element 가 화면에 노출되었는 지 여부를 간단하게 구독할 수 있는 자바스크립트 API.
	// https://heropy.blog/2019/10/27/intersection-observer/
	// https://medium.com/@pks2974/intersection-observer-%EA%B0%84%EB%8B%A8-%EC%A0%95%EB%A6%AC%ED%95%98%EA%B8%B0-fc24789799a3
	const io = new IntersectionObserver( (entries, observer) => {
		//console.log(entries[0].target.dataset.index);
		ioIndex = entries[0].target.dataset.index * 1;
		// console.log(ioIndex); // 검은색으로 출력. 문자열이라는 의미.
		// 인덱스는 숫자로 변환해주는 게 활용면에서 나음. => *1해주면 됨.
		// (숫자로 변환되면 브라우저 콘솔창에 파란색으로 출력.)
		
	}); //IntersectionObserver라는 생성자 호출
		// 여기 안에 콜백 함수(매개변수 2개) 들어감.
		// * 콜백함수란?
		// https://victorydntmd.tistory.com/48
		// https://joshua1988.github.io/web-development/javascript/javascript-asynchronous-operation/#%EC%BD%9C%EB%B0%B1-%ED%95%A8%EC%88%98%EB%A1%9C-%EB%B9%84%EB%8F%99%EA%B8%B0-%EC%B2%98%EB%A6%AC-%EB%B0%A9%EC%8B%9D%EC%9D%98-%EB%AC%B8%EC%A0%9C%EC%A0%90-%ED%95%B4%EA%B2%B0%ED%95%98%EA%B8%B0

	
   for(let i=0; i<storyElems.length; i++){
	   io.observe(storyElems[i]); // 모든 storyElems(말풍선)들이 관찰대상으로 등록됨.
	   
      // stepElems[i].setAttribute('data-index',i);  <- 방법1
      // dataset함수: data-xxxx (data- 전역)와 동일  <- 방법2
      // [형식] dataset.xxxx
      storyElems[i].dataset.index=i;
      imgElems[i].dataset.index=i;
   }
   
   function activate(){
	   currentImgElems.classList.add('visible');
   }
   
   function inactivate(){
	   currentImgElems.classList.remove('visible');
   }   
   
   // 이벤트 핸들러는 조건판별만 해주고 
   // 구체적인 세세한 기능은 다른 함수로 분리해서 처리해주는 게 좋음.
   // 스크롤 이벤트가 발생할 때 작동하는 함수 (이벤트 핸들러).
   window.addEventListener('scroll', () => {
	   
	   let story;
	   let boundingRect;
	   let temp=0;
	   
	   //for(let i=0; i<storyElems.length; i++){ // 전체 item 검사하는 loop
	   for(let i=ioIndex-1; i< ioIndex+2; i++){  // 스크롤 시 화면에 보이는 요소와 앞뒤까지 3개 요소만 판별하는 loop
		   story = storyElems[i]; // i번째 storyElems를 대입해서 사용하기 위한 변수 선언

		   // ioIndex-1부터 시작하기 때문에 storyElems[0]의 앞의 것은 storyElems[-1]이 됨. 
		   // 배열은 -1이라는 인덱스를 가질 수 없기 때문에 
		   // story가 undefined가 되면서 오류 발생. 
		   // story에 값이 없으면 for문 돌지 말고 패스 => 오류 방지.  		   
		   if(!story) continue;  
		   
		   boundingRect = story.getBoundingClientRect(); 
		   // 스크롤 시마다 DOMRect 라는 객체에 '현재 아이템'의 위치/크기를 출력
		   // console.log(boundingRect.top); // 각 story의 y위치 확인								  		   
		   // ** console.log의 활용 **
		   // https://riptutorial.com/ko/javascript/example/714/console-log----%EC%82%AC%EC%9A%A9%ED%95%98%EA%B8%B0
		   
		   temp++;
		   
		   // 윈도우 브라우저창의 10%~80% 정도 사이/구간 안에 있으면 
		   if (boundingRect.top > window.innerHeight * 0.1 && 
		       boundingRect.top < window.innerHeight * 0.8) {
				    //console.log(story.dataset.index);
				    
					if(currentImgElems) {
						//currentImgElems.classList.remove('visible');
						inactivate();
					} // if(판별) 생략하고 inactivate함수만 호출해도 됨.
					
					// 말풍선(story)이 가진 인덱스와 같은 인덱스의 imgElems
					//imgElems[story.dataset.index] // visible 클래스를 붙일 아이템/이미지
					// imgElems[story.dataset.index].classList.add('visible'); // classList 사용>> https://velog.io/@rimu/%EC%9E%90%EB%B0%94%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8A%B8-classList.add-remove-contains-toggle
					currentImgElems = imgElems[story.dataset.index];
					//currentImgElems.classList.add('visible');
					activate(); // 위의 function을 activate 함수에 담아 호출
			   } 
	   }
	   
	   console.log(temp);
   });
   // currentImgElems.classList.add('visible'); //첫번째 이미지 visible 활성화
   // 작동은 하지만, 동일 기능의 함수가 반복되니까 코드 비효율적(=후져짐)
   // 기능별로 함수 분리하는 작업 필요.
   activate();	
   
})();
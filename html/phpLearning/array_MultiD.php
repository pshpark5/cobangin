<?
$sample_array = array(
	array("apple","banana"),
	array("orange")
);

echo $sample_array[0][0]; // apple 출력
echo "<br>\n";
echo $sample_array[0][1]; // banana 출력
echo "<br>\n";
echo $sample_array[1][0]; // orange 출력
echo "<br>\n";
echo $sample_array[1][1]; // 지정된 값이 없기 때문에 에러 발생


// 다차원 배열을 선언한 후, 나중에 값을 지정하는 것도 가능
$hello = array(
array(),
array(),
array()
);

$hello[0][0]="apple";
$hello[0][1]="korea";
$hello[0][2]="1000";

$hello[1][0]="apple";
$hello[1][1]="japan";
$hello[1][2]="2000";

$hello[2][0]="avocado";
$hello[2][1]="australia";
$hello[2][2]="6000";

echo $hello[0][0].",".$hello[0][1].",".$hello[0][2]."<br>"; //apple,korea,1000
echo $hello[1][0].",".$hello[1][1].",".$hello[1][2]."<br>"; //apple,japan,2000
echo $hello[2][0].",".$hello[2][1].",".$hello[2][2]."<br>"; //avocado,australia,6000
?>
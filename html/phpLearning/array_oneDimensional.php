<?
#1차원 배열 (One-dimensional array)

// 첫번째 방법: 배열을 선언한 뒤 값을 하나씩 지정
$fruit = array();

	// 배열변수의 각 값에 접근하기 위해서는 []라는 연산자 이용
	$fruit[0]="apple";
	$fruit[1]="banana";
	$fruit[2]="orange";

// 두번째 방법: 배열을 선언하는 동시에 값도 지정ㅇ

$fruit = array("apple","banana","orange");

// 출력
echo $fruit[0];
echo "<br>\n";
echo $fruit[1];
echo "<br>\n";
echo $fruit[2];
?>
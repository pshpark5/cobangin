<?
$s_array = array("홍","박","김");

for($i=0; $i < sizeof( $s_array); $i++) 
	echo $s_array[$i]."<br>";

// sizeof 라는 함수는 배열변수에 입력된 요소의 갯수를 알아내는 함수로
// 위와 같은 경우에는 3을 출력
// 자바의  배열명.length 함수와 같음

$fEach_array = array(
	"name" => "홍길동",
	"age" => 30,
	"gender" => "남"	
);

foreach($fEach_array as $key => $value){
	echo "연관배열의 키는 ".$key.", 값은 ".$value."입니다."."<br>";
}

// foreach함수를 사용하면 연관배열에 저장된 key 와 value를 하나씩 뽑아낼 수 있다.
// 모든 값을 순서대로 하나씩 뽑아서 출력하게 됨.
?>
<!--
폼전송 실습과제
1. 이름, 성별, 생년월일, 관심사를 포함하여 4개 이상의 정보를 입력받습니다.
2. 이름의 input type은 text입니다.
3. 성별의 input type은 radio button입니다. 
4. 생년월일의 input type은 select입니다.
5. 관심사의 input type은 checkbox입니다.
 
 Bootstrap이용해서 꾸며보기
-->
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>폼전송 실습 - 로그인화면</title>

</head>
<body>
	<form action="loginSuccess.php" method="post">
	<div>
	<p>폼전송 - 실습</p>
	<p>이름 <input type="text" name="username" required autofocus></p>
	<!--
	input 태그 안에 required 를 추가해주면 해당 input을 입력하지 않고 제출을 누를 시 경고창이 뜨게 됩니다.
	required 속성은 불리언(boolean) 속성입니다.
	불리언 속성은 해당 속성을 명시하지 않으면 속성값이 자동으로 false 값을 가지게 되며, 명시하면 자동으로 true 값을 가지게 됩니다.
	[출처] https://ssungkang.tistory.com/entry/htmlinput%E1%84%8B%E1%85%B4-%E1%84%91%E1%85%B5%E1%86%AF%E1%84%89%E1%85%AE%E1%84%80%E1%85%A1%E1%86%B9%E1%84%8B%E1%85%B3%E1%86%AF-%E1%84%8C%E1%85%B5%E1%84%8C%E1%85%A5%E1%86%BC%E1%84%92%E1%85%A1%E1%84%82%E1%85%B3%E1%86%AB-required
	-->
	<p>성별 
	<!--
	radio type의 경우 name값이 일치한다면, 하나의 input에만 required 추가해주면 됨. -->
		<input type="radio" name="gender" value="남" required>남자
		<input type="radio" name="gender" value="여">여자
	</p>
	<p>생년월일
		<select id="select_year" name="year" required>
		<!--
		현재 날짜 가져오기	https://yujuwon.tistory.com/entry/PHP-%ED%98%84%EC%9E%AC-%EB%82%A0%EC%A7%9C-%EA%B0%80%EC%A0%B8%EC%98%A4%EA%B8%B0
		-->
		<?
			$today=date("Y"); //현재 날짜 중 년도 4자리만 불러오기
			for($i=1950; $i<=$today;$i++){ // 올해 년도까지만 반복
				print "<option value=$i>{$i}년";
			}
		?>
		</select>년
		<select id="select_month" name="month">
		<?
			for($i=1; $i<=12;$i++){
				print "<option value=$i>{$i}월";
			}
		?>		
		</select>월
		<select id="select_day" name="day">
		<?
			for($i=1; $i<=31;$i++){
				print "<option value=$i>{$i}일";
			}
		?>		
		</select>일
	</p>
	<p>관심사 
		<input type="checkbox" name="interest[]" value="여행">여행
		<input type="checkbox" name="interest[]" value="패션">패션
		<input type="checkbox" name="interest[]" value="음식">음식
	</p>
		<input type="submit" name="register" value="회원가입">
	</form>

	</div>
</body>
</html>
	
<!-- $_POST는 form.html에서 전송한 값을 저장하고 있는 배열변수 -->			
<?
$username = $_POST['username'];
$gender = $_POST['gender'];
$year = $_POST['year'];
$month = $_POST['month'];
$day = $_POST['day'];

// 이전 페이지에서 뭐가 넘어오는 지 (POST로 받은 내용 모두) 확인하는 코드
// print_r($_POST);

// 값이 있는 지 없는 지 확인해서 있으면 ,와 함께 출력하는 방법 => 원시적이기 때문에, 실습 해답 확인하기
// if($_POST['interest1']="")

?>	
		
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"> <!--한글 깨지지 않게 하기 위한 코드-->
		<title>폼전송 실습 - 로그인성공</title>
</head>
<body>
	<div>
		<p>회원가입 완료!</p>
		<p>
			<?echo $username."(".$gender.")" ?> 
		</p>
		<p>
			생년월일 : 
			<?echo $year."년 ".$month."월 ".$day."일"?>
		</p>
		<p>
			관심사 : 
			<?
			for($i=0; $i<sizeof( $_POST["interest"] ); $i++){
				if($i==0) echo $_POST["interest"][$i];
				else echo ", ".$_POST["interest"][$i];
			}
			?>
		</p>
	</div>
</body>
</html>
<!-- receive.php -->
 
<html>
 <head>
  <meta charset="UTF-8">
 
  <title>폼 전송 - 실습</title>
 </head>
 
 <body>
  <p>
   회원가입 완료!
  </p>
 
  <p>
   <?
    echo $_POST["name"];
 
    echo "(".$_POST["gender"].")";
   ?>
  </p>
 
  <p>
   생년월일 :
   <?
    echo $_POST["birth_year"]."년";
    echo $_POST["birth_month"]."월";
    echo $_POST["birth_date"]."일";
   ?>
  </p>
 
  <p>
   관심사 :
   <?
    for( $i = 0; $i < sizeof( $_POST["interest"] ); $i++ ){
     if ( $i == 0 ) echo $_POST["interest"][$i];
     else echo ", ".$_POST["interest"][$i];
    }
   ?>
  </p>
 </body>
</html>